<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\DeliveryBoy;
use App\Models\Manager;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductItem;
use App\Models\ProductItemFeature;
use App\Models\Shop;
use App\Models\ShopCoupon;
use App\Models\SubCategory;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => "User 1",
                'email' => "user@website.com",
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
                'avatar_url' => 'user_avatars/1.jpeg'
            ],

        ];

        $deliveryBoys = [
            [
                'name' => "Drivers 1",
                'email' => "driver@website.com",
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
                'avatar_url' => NULL,
                'mobile' => 789654123,
                'latitude' => 37.421104,
                'longitude' => -122.086951,
            ],
        ];


        foreach ($users as $user) {
            User::create($user);
        }

        foreach ($deliveryBoys as $deliveryBoy) {
            DeliveryBoy::create($deliveryBoy);
        }

    }
}
