<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpeedometersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speedometers', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('delivery_boy_id');
            $table->date('date');
            $table->integer('start');
            $table->timestamp('start_at')->nullable()->useCurrent();
            $table->text('start_file');
            $table->integer('finish')->nullable();
            $table->timestamp('finish_at')->nullable();
            $table->text('finish_file')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speedometers');
    }
}
