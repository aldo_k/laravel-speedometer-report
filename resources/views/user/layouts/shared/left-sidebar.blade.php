<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="h-100" data-simplebar>
        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul id="side-menu">


                <li>
                    <a href="{{route('user.dashboard')}}">
                        <i data-feather="home"></i>
                        <span> {{__('user.home')}} </span>
                    </a>
                </li>

                <li class="menu-title">{{__('user.navigation')}}</li>

                <li>
                    <a href="{{route('user.drivers.index')}}">
                        <i data-feather="truck"></i>
                        <span> {{__('user.drivers')}} </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('user.speedometers.index')}}">
                        <i data-feather="trending-up"></i>
                        <span> {{__('user.speedometer')}} </span>
                    </a>
                </li>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
