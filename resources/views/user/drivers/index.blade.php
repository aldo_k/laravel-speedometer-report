@extends('user.layouts.app', ['title' => 'Driver'])

@section('css')

@endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">
	<x-alert></x-alert>
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<ol class="breadcrumb m-0">
						<li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">{{env('APP_NAME')}}</a>
						</li>
						<li class="breadcrumb-item active">{{__('user.driver')}}</li>
					</ol>
				</div>
				<h4 class="page-title">{{__('user.driver')}}</h4>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<div class="row ">
				<div class="col-12">
					<div class="row mb-2">
						<div class="col-sm-4">
							{!! $drivers->onEachSide(5)->links() !!}
						</div>
						<div class="col-sm-8">
							<div class="text-sm-right">
								<a href="javascript:history.go(0)">
									<i class="mdi mdi-refresh mr-3"
									style="font-size: 22px"></i>
								</a>
								@if($user->level == 'admin')
								<a type="button" href="{{route('user.drivers.add')}}" class="btn btn-primary waves-effect waves-light mb-2 text-white">
									{{__('user.add_drivers')}}
								</a>
								@endif
							</div>
						</div><!-- end col-->
					</div>

					@if($drivers->count()>0)
					<div class="table-responsive">
						<table class="table table-centered table-nowrap table-striped table-hover mb-0">
							<thead class="thead-light">
								<tr>
									<th>ID</th>
									<th>{{__('user.image')}}</th>
									<th>{{__('user.motorcycle_license_plate')}}</th>
									<th>{{__('user.name')}}</th>
									@if($user->level == 'admin')
									<th>{{__('user.mobile')}}</th>
									@endif
									<th>{{__('user.email')}}</th>
									<th>{{__('user.status')}}</th>
									<th class="text-right">{{__('user.total_km')}}</th>
									@if($user->level == 'admin')
									<th style="width: 82px;">{{__('user.action')}}</th>
									@endif
								</tr>
							</thead>
							<tbody>
								@foreach($drivers as $driver)
								<tr>
									<td>{{$loop->iteration + (10 * ((request('page') ?? 1) - 1))}}
									</td>
									<td>

										<img src="{{\App\Helpers\TextUtil::getImageUrl($driver['avatar_url'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" alt="image" class="img-fluid avatar-sm rounded-circle">
									</td>
									<td>{{$driver->motorcycle_license_plate}}</td>
									<td>{{$driver->name}}</td>
									@if($user->level == 'admin')
									<td>{{$driver->mobile}}</td>
									@endif
									<td>{{$driver->email}}</td>
									<td>
										@if($driver->is_offline)
										<span class="bg-danger mr-1" style="border-radius: 50%;width: 8px;height: 8px;  display: inline-block;"></span> {{__('user.offline')}}
										@else
										<span class="bg-primary mr-1" style="border-radius: 50%;width: 8px;height: 8px;  display: inline-block;"></span> {{__('user.online')}}
										@endif

									</td>
									<td class="text-right" width="5%">{{number_format($driver->total_km,2)}} Km</td>
									@if($user->level == 'admin')
									<td>
										<a href="{{route('user.drivers.edit',['id'=>$driver->id])}}" class="btn btn-primary">
											<i class="mdi mdi-pencil-outline"></i>
										</a>
										<a href="{{route('user.drivers.destroy',['id'=>$driver->id])}}?secret={{ password_hash($driver->id, PASSWORD_DEFAULT) }}" class="btn btn-danger" onclick="return confirm('{{__('user.sure_delete?')}}');">
											<i class="mdi mdi-delete"></i>
										</a>
									</td>
									@endif
								</tr>
								@endforeach

							</tbody>
						</table>
						<hr>
						<div class="page-area">
							<table>
								<tfoot class="border-top">

								</tfoot>
							</table>
						</div>
					</div>
					@else
					<h3>{{__('user.you_have_not_any_driver')}}</h3>
					@endif

				</div>
			</div>


		</div>
	</div>
</div>

@endsection

@section('script')

@endsection
