@extends('user.layouts.app', ['title' => 'Update Drivers'])

@section('css')
@endsection

@section('content')

{{-- Nama
Email
Password
Plat No Polisi
No HP
Foto --}}

<!-- Start Content-->
<div class="container-fluid">
    <x-alert></x-alert>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">{{env('APP_NAME')}}</a>
                        </li>
                        <li class="breadcrumb-item"><a
                            href="{{route('user.drivers.index')}}">{{__('user.drivers')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{__('user.create')}}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{__('user.update_drivers')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('user.drivers.update',['id'=>$deliveryBoy->id])}}" method="POST" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <label for="driver_name" class="col-sm-3 col-form-label">{{__('user.driver_name')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control  @if($errors->has('driver_name')) is-invalid @endif " id="driver_name" placeholder="{{__('user.driver_name')}}" name="driver_name" value="{{ $deliveryBoy->name }}" required>
                                @if($errors->has('driver_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('driver_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="motorcycle_license_plate" class="col-sm-3 col-form-label">{{__('user.motorcycle_license_plate')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control  @if($errors->has('motorcycle_license_plate')) is-invalid @endif " id="motorcycle_license_plate" placeholder="{{__('user.motorcycle_license_plate')}}" name="motorcycle_license_plate" value="{{ $deliveryBoy->motorcycle_license_plate }}" required>
                                @if($errors->has('motorcycle_license_plate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('motorcycle_license_plate') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="driver_phone" class="col-sm-3 col-form-label">{{__('user.driver_phone')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control  @if($errors->has('driver_phone')) is-invalid @endif " id="driver_phone" placeholder="{{__('user.driver_phone')}}" name="driver_phone" value="{{ $deliveryBoy->mobile }}" required>
                                @if($errors->has('driver_phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('driver_phone') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="driver_email" class="col-sm-3 col-form-label">{{__('user.driver_email')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control  @if($errors->has('driver_email')) is-invalid @endif " id="driver_email" placeholder="{{__('user.driver_email')}}" name="driver_email" value="{{ $deliveryBoy->email }}" required>
                                @if($errors->has('driver_email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('driver_email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="driver_password" class="col-sm-3 col-form-label">{{__('user.driver_password')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control  @if($errors->has('driver_password')) is-invalid @endif " id="driver_password" placeholder="{{__('user.driver_password')}} ({{__('user.optional')}})" name="driver_password">
                                @if($errors->has('driver_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('driver_password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-between mb-4 mx-3">

                        <button type="button" onclick="window.history.back();" class="btn btn-outline-secondary">
                            {{__('user.back')}}
                        </button>
                        <button type="submit"class="btn btn-primary">
                            <i class="mdi mdi-pencil-outline mr-1"></i>
                            {{__('user.update')}}
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div> <!-- container -->

@endsection

@section('script')
@endsection
