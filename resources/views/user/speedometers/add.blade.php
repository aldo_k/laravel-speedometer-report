@extends('user.layouts.app', ['title' => 'Create Speedometer'])
@section('css')
<link href="{{asset('assets/libs/select2/select2.min.css')}}"" rel="stylesheet" />
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"" rel="stylesheet" />
@endsection

@section('content')

{{-- Nama
Email
Password
Plat No Polisi
No HP
Foto --}}

<!-- Start Content-->
<div class="container-fluid">
    <x-alert></x-alert>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">{{env('APP_NAME')}}</a>
                        </li>
                        <li class="breadcrumb-item"><a
                            href="{{route('user.speedometers.index')}}">{{__('user.speedometer')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{__('user.create')}}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{__('user.create_speedometer')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('user.speedometers.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="delivery_boy_id" class="col-sm-3 col-form-label">{{__('user.drivers')}}</label>
                            <div class="col-sm-9">
                                <select name="delivery_boy_id" class="form-control select2" @if($errors->has('delivery_boy_id')) is-invalid @endif required>
                                    @foreach($deliveryBoys as $deliveryBoy)
                                    <option value="{{ $deliveryBoy->id }}">{{ $deliveryBoy->motorcycle_license_plate }} ({{ $deliveryBoy->name }})</option>
                                    @endforeach
                                </select>
                                @if($errors->has('delivery_boy_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('delivery_boy_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-sm-3 col-form-label">{{__('user.date')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control basic-datepicker @if($errors->has('date')) is-invalid @endif "
                                id="date" placeholder="{{__('user.date')}}"
                                name="date" required>
                                @if($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start" class="col-sm-3 col-form-label">{{__('user.total_km')}}</label>
                            <div class="col-sm-9">
                                <input type="number" step="any" class="form-control  @if($errors->has('start')) is-invalid @endif "
                                id="start" placeholder="{{__('user.total_km')}}"
                                name="start" required>
                                @if($errors->has('start'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_file" class="col-sm-3 col-form-label">{{__('user.speedometer')}}</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control  @if($errors->has('start_file')) is-invalid @endif "
                                id="start_file" placeholder="{{__('user.speedometer')}}"
                                name="start_file" required>
                                @if($errors->has('start_file'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start_file') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-between mb-4 mx-3">

                        <button type="button" onclick="window.history.back();" class="btn btn-outline-secondary">
                            {{__('user.back')}}
                        </button>
                        <button type="submit"class="btn btn-primary">
                            <i class="mdi mdi-plus mr-1"></i>
                            {{__('user.create')}}
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div> <!-- container -->

@endsection

@section('script')
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        $('.basic-datepicker').datepicker({format: 'yyyy-mm-dd', autoclose: true});
    });
</script>
@endsection
