@extends('user.layouts.app', ['title' => 'Driver'])

@section('css')
<link href="{{asset('assets/libs/select2/select2.min.css')}}"" rel="stylesheet" />
@endsection

@section('content')

<!-- Start Content-->
<div class="container-fluid">
	<x-alert></x-alert>
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<ol class="breadcrumb m-0">
						<li class="breadcrumb-item">
							<a href="{{route('user.dashboard')}}">
								{{env('APP_NAME')}}
							</a>
						</li>
						<li class="breadcrumb-item active">{{__('user.speedometer')}}</li>
					</ol>
				</div>
				<h4 class="page-title">{{__('user.speedometer')}}</h4>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<div class="row ">
				<div class="col-md-12 col-xl-12">
					<div class="form-group row">
						<label for="delivery_boy_id" class="col-sm-3 col-form-label">{{__('user.drivers')}}</label>
						<div class="col-sm-9">
							<select name="delivery_boy_id" class="form-control select2" @if($errors->has('delivery_boy_id')) is-invalid @endif>
								<option value="">Semua Driver</option>
								@foreach($deliveryBoys as $deliveryBoy)
								<option value="{{ $deliveryBoy->id }}" {{ $id==$deliveryBoy->id ? 'selected' : '' }}>{{ $deliveryBoy->motorcycle_license_plate }} ({{ $deliveryBoy->name }})</option>
								@endforeach
							</select>
							@if($errors->has('delivery_boy_id'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('delivery_boy_id') }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="row mb-2">
						<div class="col-sm-4">
							{!! $speedometers->onEachSide(5)->links() !!}
						</div>
						<div class="col-sm-8">
							<div class="text-sm-right">
								<a href="javascript:history.go(0)">
									<i class="mdi mdi-refresh mr-3" style="font-size: 22px"></i>
								</a>
								@if($user->level == 'admin')
								<a type="button" href="{{route('user.speedometers.add')}}" class="btn btn-primary waves-effect waves-light mb-2 text-white">
									{{__('user.add_speedometers')}}
								</a>
								@endif
							</div>
						</div><!-- end col-->
					</div>

					@if($speedometers->count()>0)
					<div class="table-responsive">
						<table class="table table-centered table-nowrap table-striped table-hover mb-0">
							<thead class="thead-light">
								<tr>
									<th>ID</th>
									<th>{{__('user.motorcycle_license_plate')}}</th>
									<th>{{__('user.name')}}</th>
									<th>{{__('user.date')}}</th>
									{{-- <th>{{__('user.speedometer_start')}}</th> --}}
									{{-- <th>{{__('user.image')}}</th> --}}
									{{-- <th>{{__('user.speedometer_finish')}}</th> --}}
									<th>{{__('user.image')}}</th>
									<th class="text-right">{{__('user.total_km')}}</th>
									@if($user->level == 'admin')
									<th style="width: 82px;">{{__('user.action')}}</th>
									@endif
								</tr>
							</thead>
							<tbody>
								@foreach($speedometers as $speedometer)
								<tr>
									<td>{{$loop->iteration + (10 * ((request('page') ?? 1) - 1))}}
									</td>
									<td>{{$speedometer->motorcycle_license_plate}}</td>
									<td>{{$speedometer->name}}</td>
									<td>{{$speedometer->date->format('d M Y')}}</td>
									{{-- <td>{{ sprintf("%06s", $speedometer->start) }} /KM</td> --}}
									{{-- <td>
										<a href="{{\App\Helpers\TextUtil::getImageUrl($speedometer['start_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" target="_blank">
											<img src="{{\App\Helpers\TextUtil::getImageUrl($speedometer['start_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" alt="image" class="img-fluid avatar-sm rounded-circle">
										</a>
									</td> --}}
									{{-- <td>{{sprintf("%06s", $speedometer->finish)}} /KM</td> --}}
									<td>
										<a href="{{\App\Helpers\TextUtil::getImageUrl($speedometer['finish_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" target="_blank">
											<img src="{{\App\Helpers\TextUtil::getImageUrl($speedometer['finish_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" alt="image" class="img-fluid avatar-sm rounded-circle">
										</a>
									</td>
									<td class="text-right">{{number_format((($speedometer->finish - $speedometer->start)/10),2)}} KM</td>
									@if($user->level == 'admin')
									<td>
										<a href="{{route('user.speedometers.edit',['id'=>$speedometer->id])}}" class="btn btn-primary">
											<i class="mdi mdi-pencil-outline"></i>
										</a>
										<a href="{{route('user.speedometers.destroy',['id'=>$speedometer->id])}}?secret={{ password_hash($speedometer->id, PASSWORD_DEFAULT) }}" class="btn btn-danger" onclick="return confirm('{{__('user.sure_delete?')}}');">
											<i class="mdi mdi-delete"></i>
										</a>
									</td>
									@endif
								</tr>
								@endforeach

							</tbody>
						</table>
						<hr>
						<div class="page-area">
							<table>
								<tfoot class="border-top">

								</tfoot>
							</table>
						</div>
					</div>
					@else
					<h3>{{__('user.you_have_not_any_driver')}}</h3>
					@endif

				</div>
			</div>


		</div>
	</div>
</div>

@endsection

@section('script')
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".select2").unbind();

		$('.select2').select2();
		$(`.select2`).on('change', function(event) {
			let id = $(this).val();
			window.location.href = `?id=${id}`;
		});
	});
</script>
@endsection
