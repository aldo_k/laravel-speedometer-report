@extends('user.layouts.app', ['title' => 'Update Drivers'])

@section('css')
<link href="{{asset('assets/libs/select2/select2.min.css')}}"" rel="stylesheet" />
<link href="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"" rel="stylesheet" />
@endsection

@section('content')

{{-- Nama
Email
Password
Plat No Polisi
No HP
Foto --}}

<!-- Start Content-->
<div class="container-fluid">
    <x-alert></x-alert>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">{{env('APP_NAME')}}</a>
                        </li>
                        <li class="breadcrumb-item"><a
                            href="{{route('user.speedometers.index')}}">{{__('user.speedometers')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{__('user.create')}}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{__('user.update_speedometers')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('user.speedometers.update',['id'=>$speedometer->id])}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="delivery_boy_id" class="col-sm-3 col-form-label">{{__('user.drivers')}}</label>
                            <div class="col-sm-9">
                                <select name="delivery_boy_id" class="form-control select2" @if($errors->has('delivery_boy_id')) is-invalid @endif required>
                                    <option value="{{ $speedometer->delivery_boy_id }}" selected="selected">{{ $speedometer->motorcycle_license_plate }} ({{ $speedometer->name }})</option>
                                    @foreach($deliveryBoys as $deliveryBoy)
                                    <option value="{{ $deliveryBoy->id }}">{{ $deliveryBoy->motorcycle_license_plate }} ({{ $deliveryBoy->name }})</option>
                                    @endforeach
                                </select>
                                @if($errors->has('delivery_boy_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('delivery_boy_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-sm-3 col-form-label">{{__('user.date')}}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control basic-datepicker @if($errors->has('date')) is-invalid @endif " id="date" placeholder="{{__('user.date')}}" name="date" value="{{ $speedometer->date->format('Y-m-d') }}" required>
                                @if($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start" class="col-sm-3 col-form-label">{{__('user.start')}}</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control  @if($errors->has('start')) is-invalid @endif " id="start" placeholder="{{__('user.start')}}" name="start" value="{{ $speedometer->start }}"  required>
                                @if($errors->has('start'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_file" class="col-sm-3 col-form-label">{{__('user.start_file')}}</label>
                            <div class="col-sm-1">
                                <a href="{{\App\Helpers\TextUtil::getImageUrl($speedometer['start_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" target="_blank">
                                    <img src="{{\App\Helpers\TextUtil::getImageUrl($speedometer['start_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" alt="image" class="img-fluid avatar-sm rounded-circle">
                                </a>
                            </div>
                            <div class="col-sm-8">
                                <input type="file" class="form-control  @if($errors->has('start_file')) is-invalid @endif " id="start_file" placeholder="{{__('user.start_file')}}" name="start_file">
                                @if($errors->has('start_file'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start_file') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="finish" class="col-sm-3 col-form-label">{{__('user.finish')}}</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control  @if($errors->has('finish')) is-invalid @endif " id="finish" placeholder="{{__('user.finish')}}" name="finish" value="{{ $speedometer->finish }}" required>
                                @if($errors->has('finish'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('finish') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="finish_file" class="col-sm-3 col-form-label">{{__('user.finish_file')}}</label>
                            <div class="col-sm-1">
                                <a href="{{\App\Helpers\TextUtil::getImageUrl($speedometer['finish_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" target="_blank">
                                    <img src="{{\App\Helpers\TextUtil::getImageUrl($speedometer['finish_file'],\App\Helpers\TextUtil::$PLACEHOLDER_AVATAR_URL)}}" alt="image" class="img-fluid avatar-sm rounded-circle">
                                </a>
                            </div>
                            <div class="col-sm-8">
                                <input type="file" class="form-control  @if($errors->has('finish_file')) is-invalid @endif " id="finish_file" placeholder="{{__('user.finish_file')}}" name="finish_file">
                                @if($errors->has('finish_file'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('finish_file') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-between mb-4 mx-3">

                        <button type="button" onclick="window.history.back();" class="btn btn-outline-secondary">
                            {{__('user.back')}}
                        </button>
                        <button type="submit"class="btn btn-primary">
                            <i class="mdi mdi-pencil-outline mr-1"></i>
                            {{__('user.update')}}
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div> <!-- container -->

@endsection

@section('script')
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        $('.basic-datepicker').datepicker({format: 'yyyy-mm-dd', autoclose: true});
    });
</script>
@endsection
