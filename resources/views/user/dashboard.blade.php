
@extends('user.layouts.app', ['title' => 'Dashboard'])

@section('css')
<link href="{{asset('assets/libs/select2/select2.min.css')}}"" rel="stylesheet" />
@endsection

@section('content')
<!-- Start Content-->
<div class="container-fluid">
    <x-alert></x-alert>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">{{__('user.home')}}</li>
                    </ol>
                </div>
                <h4 class="page-title">{{__('user.home')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                <i class="fe-users font-22 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="text-end text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $total_delivery_boys }}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Driver</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                <i class="fe-trending-up font-22 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="text-end text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ (float) $total_km }}</span> KM</h3>
                                <p class="text-muted mb-1 text-truncate">Total Perjalanan</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div>
        <div class="col-md-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12 col-xl-12">
                        <div class="form-group row">
                            <label for="delivery_boy_id" class="col-sm-3 col-form-label">{{__('user.drivers')}}</label>
                            <div class="col-sm-9">
                                <select name="delivery_boy_id" class="form-control select2" @if($errors->has('delivery_boy_id')) is-invalid @endif>
                                    <option value="">Semua Driver</option>
                                    @foreach($deliveryBoys as $deliveryBoy)
                                    <option value="{{ $deliveryBoy->id }}" {{ $id==$deliveryBoy->id ? 'selected' : '' }}>{{ $deliveryBoy->motorcycle_license_plate }} ({{ $deliveryBoy->name }})</option>
                                    @endforeach
                                </select>
                                @if($errors->has('delivery_boy_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('delivery_boy_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        {{$chart->container()}}
                    </div>
                </div>
            </div>
        </div>


    </div>
</div> <!-- container -->
@endsection

@section('script')
<script src="{{ $chart->cdn() }}"></script>
{{ $chart->script() }}

<!-- Plugins js-->
<script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{asset('assets/libs/selectize/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select2").unbind();

        $('.select2').select2();
        $(`.select2`).on('change', function(event) {
            let id = $(this).val();
            window.location.href = `?id=${id}`;
        });
    });
</script>
@endsection
