<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Models\DeliveryBoy;
use App\Models\Speedometer;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Storage;

class SpeedometerController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        if (request('id')) {
            $speedometers = Speedometer::select('*', 'speedometers.id as id')
            ->join('delivery_boys','delivery_boys.id', 'speedometers.delivery_boy_id')
            ->where('delivery_boy_id', request('id'))
            ->orderBy('speedometers.updated_at', 'DESC')
            ->paginate(30);
        } else {
            $speedometers = Speedometer::select('*', 'speedometers.id as id')
            ->join('delivery_boys','delivery_boys.id', 'speedometers.delivery_boy_id')
            ->orderBy('speedometers.updated_at', 'DESC')
            ->paginate(30);
        }

        $deliveryBoys = DeliveryBoy::all();

        return view('user.speedometers.index')->with([
            'speedometers' => $speedometers,
            'user'    => $user,
            'deliveryBoys' => $deliveryBoys,
            'id' => request('id'),
        ]);
    }


    public function create()
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/speedometers');
        }

        $deliveryBoys = DeliveryBoy::all();

        return view('user.speedometers.add')->with([
            'deliveryBoys' => $deliveryBoys
        ]);
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/speedometers');
        }

        $this->validate($request, [
            'delivery_boy_id' => 'required|exists:delivery_boys,id',
            'date' => 'required|date_format:Y-m-d',
            'start' => 'required|regex:/^-?[0-9]+(?:.[0-9]{1,2})?$/',
            'start_file' => 'required|mimes:jpg,jpeg,png|max:5120|min:1',
        ],[
            'delivery_boy_id.required' => 'Driver Harus Dipilih',
            'date.required' => 'Tanggal Harus Dipilih',
            'start.required' => 'Angka Speedometer Harus Diisi',
            'start.regex' => 'Angka Speedometer Harus Angka (Bisa Decimal)',
            'start_file.required' => 'Foto Speedometer Harus Diisi',
        ]);

        $speedometerExist = Speedometer::where(['date' => $request->date, 'delivery_boy_id' => $request->delivery_boy_id])->count();

        if($speedometerExist){
            return redirect(route('user.speedometers.add'))->with('message', 'Data Sudah Pernah Diinput, Silahkan Perbarui Data Tersebut');
        }

        $speedometerBefore = Speedometer::where('date', '<>', date('Y-m-d'))
        ->where('delivery_boy_id', '=', $request->delivery_boy_id)
        ->orderBy('date', 'DESC')
        ->first();

        $speedometer = new Speedometer();

        // If Exist Speedometer Before Today
        if ($speedometerBefore) {
            $speedometer->delivery_boy_id = $request->delivery_boy_id;
            $speedometer->date = $request->date;

            // Get Old Finish Data Speedometer To Insert Into Start Data Speedometer
            $speedometer->start = $speedometerBefore->finish;
            $speedometer->start_at = $speedometerBefore->finish_at;
            $speedometer->start_file = $speedometerBefore->finish_file;

            $finish = ((int)$speedometerBefore->finish) + ($request->start*10);

            $speedometer->finish = sprintf("%06s", $finish);
            $speedometer->finish_at = now();

            if($request->file('start_file')){
                $start_file = Str::uuid() . ".jpg";
                $data = $request->file('start_file');
                $data->move(storage_path('app/public/speedometers'), $start_file);
                $speedometer->finish_file = "/speedometers/{$start_file}";
            }

        } else {

            $speedometer->delivery_boy_id = $request->delivery_boy_id;
            $speedometer->date = $request->date;
            $speedometer->start = '000000';

            $finish = $request->start*10;
            $speedometer->finish = sprintf("%06s", $finish);

            if($request->file('start_file')){
                $start_file = Str::uuid() . ".jpg";
                $data = $request->file('start_file');
                $data->move(storage_path('app/public/speedometers'), $start_file);
                $speedometer->start_file = "/speedometers/{$start_file}";
                $speedometer->finish_file = "/speedometers/{$start_file}";
            }
        }

        $speedometer->save();

        return redirect(route('user.speedometers.add'))->with('message', 'Speedometer Berhasil Disimpan, Silahkan Input Speedometer Lainnnya!');
    }

    public function edit($id)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/speedometers');
        }

        $deliveryBoys = DeliveryBoy::all();
        $speedometer = Speedometer::select('*', 'speedometers.id as id')->join('delivery_boys','delivery_boys.id', 'speedometers.delivery_boy_id')->findOrFail($id);

        return view('user.speedometers.edit')->with([
            'deliveryBoys' => $deliveryBoys,
            'speedometer' => $speedometer
        ]);

    }


    public function update(Request $request, $id)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/speedometers');
        }

        $this->validate($request, [
            'delivery_boy_id' => 'required|exists:delivery_boys,id',
            'date' => 'required|date_format:Y-m-d',
            'start' => 'required|numeric',
            'start_file' => 'nullable|mimes:jpg,jpeg,png|max:5120|min:1',
            'finish' => 'required|numeric',
            'finish_file' => 'nullable|mimes:jpg,jpeg,png|max:5120|min:1',
        ]);

        $speedometer = Speedometer::find($id);

        $speedometer->delivery_boy_id = $request->delivery_boy_id;
        $speedometer->date = $request->date;
        $speedometer->start = $request->start;
        $speedometer->finish = $request->finish;

        if($request->file('start_file')){

            $start_file = Str::uuid() . ".jpg";
            $data = $request->file('start_file');

            $data->move(storage_path('app/public/speedometers'), $start_file);

            $speedometer->start_file = "/speedometers/{$start_file}";
        }

        if($request->file('finish_file')){

            $finish_file = Str::uuid() . ".jpg";
            $data = $request->file('finish_file');

            $data->move(storage_path('app/public/speedometers'), $finish_file);

            $speedometer->finish_file = "/speedometers/{$finish_file}";
        }

        if ($speedometer->save()) {
            return redirect(route('user.speedometers.index'))->with('message', 'Driver updated succesfully');
        }

        return redirect(route('admin.categories.index'))->with('error', 'Driver not updated');
    }


    public function destroy(Request $request, $id)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/speedometers');
        }

        $speedometer = Speedometer::find($id);

        if(!password_verify($id, $request->secret)){
            return redirect(route('user.speedometers.index'))->with('error', 'Something Wrong!');
        }

        if ($speedometer->delete()) {
            return redirect(route('user.speedometers.index'))->with('message', 'Speedometer deleted succesfully');
        }

        return redirect(route('user.speedometers.index'))->with('error', 'Something Wrong!');
    }

}
