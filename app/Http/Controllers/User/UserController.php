<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use App\Models\DeliveryBoy;
use App\Models\Speedometer;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:user');
    }


    public function index()
    {

        $deliveryBoys = DeliveryBoy::all();

        $total_delivery_boys = DeliveryBoy::count();
        $summary = Speedometer::select(
            DB::raw('SUM((`finish` - `start`)/10) as total_km'),
        )->first();

        $speedometer_summary = Speedometer::select(
            DB::raw('CONCAT(DATE_FORMAT(date, "%d %b"), " (", COUNT(id), ")") as date_axis'),
            DB::raw("REPLACE(FORMAT(SUM((`finish` - `start`)/10), 2),',','') as total_km"),
            DB::raw("REPLACE(FORMAT((SUM((`finish` - `start`)/10)/COUNT(id)), 2),',','') as total_km_avg"),
        );

        if (request('id')) {
            $speedometer_summary = $speedometer_summary->where('delivery_boy_id', request('id'));
        }

        $speedometer_summary = $speedometer_summary->groupBy('date');

        $totalKMByDays = $speedometer_summary->pluck('total_km');
        $totalKMAvgByDays = $speedometer_summary->pluck('total_km_avg');

        $xAxis = $speedometer_summary->pluck('date_axis')->toArray();

        // dd($totalKMByDays);

        $chart = new LarapexChart();

        $chart->setType('line')
        ->setTitle('Total KM Daily')
        ->setXAxis($xAxis)
        ->setDataset([
            [
                'name'  =>  'Total KM',
                'data'  =>  $totalKMByDays,
            ],
            [
                'name'  =>  'AVG KM',
                'data'  =>  $totalKMAvgByDays,
            ],
        ]);

        return view('user.dashboard')->with([
            'chart' => $chart,
            'total_km' => $summary->total_km,
            'total_delivery_boys' => $total_delivery_boys,
            'deliveryBoys' => $deliveryBoys,
            'id' => request('id'),
        ]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
    }

    public function edit()
    {
        $user = auth()->user();

        return view('user.auth.setting', [
            'user' => $user
        ]);
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $this->validate($request,
            [
                'name' => 'required',
            ]);


        if ($request->hasFile('image')) {
            User::updateUserAvatar($request, $user->id);
        }

        if(isset($request->password)){
            $user->password = Hash::make($request->password);
        }

        $user->name = $request->get('name');
        if ($user->save()) {
            return redirect()->back()->with([
                'message' => 'Profile updated'
            ]);
        }
        return redirect()->back()->with([
            'error' => 'Something wrong'
        ]);

    }

    public function destroy($id)
    {

    }

    public function updateLocale($langCode){
        $user = auth()->user();
        $user->locale = $langCode;
        if($user->save()){
            return redirect()->back()->with([
                'message' => 'Language changed'
            ]);
        }else{
            return redirect()->back()->with([
                'error' => 'Something wrong'
            ]);
        }

    }

}
