<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Models\DeliveryBoy;
use App\Models\Speedometer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DeliveryBoyController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $speedometer_summary = Speedometer::select(
            'delivery_boy_id',
            DB::raw('SUM((`finish` - `start`)/10) as total_km'),
        )
        ->groupBy('delivery_boy_id');


        $drivers = DeliveryBoy::leftJoinSub($speedometer_summary, 'speedometer_summary', function ($join) {
            $join->on('delivery_boys.id', '=', 'speedometer_summary.delivery_boy_id');
        })
        ->orderBy('total_km', 'DESC')
        ->paginate(100);

        return view('user.drivers.index')->with([
            'drivers' => $drivers,
            'user'    => $user,
        ]);
    }


    public function create()
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/drivers');
        }

        return view('user.drivers.add');
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/drivers');
        }

        $this->validate($request, [
            'driver_name' => 'required',
            'motorcycle_license_plate' => 'required',
            'driver_phone' => 'required',
            'driver_email' => 'required|email|unique:delivery_boys,email',
            'driver_password' => 'required'
        ]);

        $deliveryBoy = new DeliveryBoy();
        $deliveryBoy->name = $request->driver_name;
        $deliveryBoy->motorcycle_license_plate = $request->motorcycle_license_plate;
        $deliveryBoy->mobile = $request->driver_phone;
        $deliveryBoy->email = $request->driver_email;
        $deliveryBoy->password = bcrypt($request->driver_password);
        $deliveryBoy->save();

        return redirect(route('user.drivers.index'))->with('message', 'Driver added succesfully');
    }

    public function edit($id)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/drivers');
        }

        $deliveryBoy = DeliveryBoy::findOrFail($id);

        return view('user.drivers.edit')->with([
            'deliveryBoy' => $deliveryBoy
        ]);

    }


    public function update(Request $request, $id)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/drivers');
        }

        $this->validate($request, [
            'driver_name' => 'required',
            'motorcycle_license_plate' => 'required',
            'driver_phone' => 'required',
            'driver_email' => 'required|email|unique:delivery_boys,email,' . $id,
            'driver_password' => ''
        ]);

        $deliveryBoy = DeliveryBoy::find($id);
        $deliveryBoy->name = $request->driver_name;
        $deliveryBoy->motorcycle_license_plate = $request->motorcycle_license_plate;
        $deliveryBoy->mobile = $request->driver_phone;
        $deliveryBoy->email = $request->driver_email;

        if ($request->driver_password) {
            $deliveryBoy->password = bcrypt($request->driver_password);
        }

        if ($deliveryBoy->save()) {
            return redirect(route('user.drivers.index'))->with('message', 'Driver updated succesfully');
        }

        return redirect(route('admin.categories.index'))->with('error', 'Driver not updated');
    }


    public function destroy(Request $request, $id)
    {
        $user = auth()->user();

        if ($user->level!='admin') {
            return redirect('/user/drivers');
        }

        $deliveryBoy = DeliveryBoy::find($id);

        if(!password_verify($id, $request->secret)){
            return redirect(route('user.drivers.index'))->with('error', 'Something Wrong!');
        }

        if ($deliveryBoy) {

            $totalSpeedometer = Speedometer::where('delivery_boy_id', $deliveryBoy->id)->count();

            if($totalSpeedometer){
                return redirect(route('user.drivers.index'))->with('error', 'Driver can not be deleted');
            }

            $deliveryBoy->delete();

            return redirect(route('user.drivers.index'))->with('message', 'Driver deleted succesfully');
        }

        return redirect(route('user.drivers.index'))->with('error', 'Something Wrong!');
    }

}
