<?php

namespace App\Http\Controllers\Api\v1\DeliveryBoy;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FCMController;
use App\Models\DeliveryBoy;
use App\Models\Speedometer;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Storage;

class SpeedometerController extends Controller
{
	public function index()
	{
		$deliveryBoyId = auth()->user()->id;

		return Speedometer::where('delivery_boy_id', '=', $deliveryBoyId)
		->orderBy('status', 'ASC')
		->orderBy('date', 'DESC')
		->limit(31)
		->get();
	}

	public function store(Request $request)
	{
		$this->validate($request,
			[
				'start' => 'required|numeric',
				'start_file' => 'required',
			],
			[
				'start.required' => 'Angka Speedometer Harus Diisi',
				'start.numeric' => 'Angka Speedometer Harus Angka',
				'start_file.required' => 'Foto Speedometer Harus Diisi',
			]);

		$deliveryBoyId = auth()->user()->id;

		$findToday = Speedometer::where('date', date('Y-m-d'))->where('delivery_boy_id', '=', $deliveryBoyId)->first();
		if ($findToday) {
			return response(['errors' => ['Kamu Hanya Bisa Input 1 Data Speedometer Perhari..']], 422);
		}

		$speedometerBefore = Speedometer::where('date', '<>', date('Y-m-d'))
		->where('delivery_boy_id', '=', $deliveryBoyId)
		->orderBy('date', 'DESC')
		->first();

		$speedometer = new Speedometer();

		$speedometer->date = date('Y-m-d');
		$speedometer->status = 0;
		$speedometer->delivery_boy_id = $deliveryBoyId;

		if ($speedometerBefore) {
			if ($speedometerBefore->finish == null) {
				return response(['errors' => ['Data Hari Sebelumnya Belum Selesai Diisi!']], 422);
			}

			if ($speedometerBefore->finish > $request->start) {
				return response(['errors' => ['Angka Speedometer Tidak Valid!']], 422);
			}

			// Get Old Finish Data Speedometer To Insert Into Start Data Speedometer
			$speedometer->start = $speedometerBefore->finish;
			$speedometer->start_at = $speedometerBefore->finish_at;
			$speedometer->start_file = $speedometerBefore->finish_file;

			$speedometer->finish = $request->start;
			$speedometer->finish_at = now();

			if(isset($request->start_file)){
				$url = "speedometers/" . Str::uuid() . ".jpg";
				$data = base64_decode($request->start_file);
				Storage::disk('public')->put($url, $data);
				$speedometer->finish_file = $url;
			}
		} else {
			$speedometer->start = $request->start;

			if(isset($request->start_file)){
				$url = "speedometers/" . Str::uuid() . ".jpg";
				$data = base64_decode($request->start_file);
				Storage::disk('public')->put($url, $data);
				$speedometer->start_file = $url;
			}

		}

		if ($speedometer->save()) {
			return response(['message' => ['Upload Speedometer Pagi Berhasil Diterima.']], 200);
		}

		return response(['errors' => ['Terjadi Kesalahan, Silahkan Ulangi Beberapa Saat Lagi!']], 422);
	}

	public function show($id)
	{
		$deliveryBoyId = auth()->user()->id;

		return Speedometer::where('delivery_boy_id', '=', $deliveryBoyId)
		->where('status', '<>', 1)
		->findOrFail($id);

	}

	public function update(Request $request, $id)
	{
		$deliveryBoyId = auth()->user()->id;

		$speedometer = Speedometer::where('delivery_boy_id', '=', $deliveryBoyId)
		->findOrFail($id);

		if ($speedometer->status == 1) {
			return response(['errors' => ['Maaf, Speedometer Kamu Udah Diverifikasi!']], 422);
		}

		if ($speedometer->start > $request->finish) {
			return response(['errors' => ["Angka Speedometer Tidak Valid! \n- Angka Sebelumnya {$speedometer->start}"]], 422);
		}

		if(isset($request->finish_file)){

			$url = "speedometers/" . Str::uuid() . ".jpg";
			$data = base64_decode($request->finish_file);

			Storage::disk('public')->put($url, $data);

			$speedometer->finish_file = $url;
		}

		$speedometer->finish = $request->finish;
		$speedometer->finish_at = now();
		$speedometer->status = 0;

		if ($speedometer->save()) {
			return response(['message' => ['Upload Speedometer Sore Berhasil Diterima.']], 200);
		}
		return response(['errors' => ['Terjadi Kesalahan, Silahkan Ulangi Beberapa Saat Lagi!']], 422);
	}


	public function destroy($id)
	{

	}
}
