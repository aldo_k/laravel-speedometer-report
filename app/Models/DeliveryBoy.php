<?php

namespace App\Models;

use App\Notifications\AdminResetPasswordNotification;
use App\Notifications\DeliveryBoyResetPasswordNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * @method static where(string $string, string $string1, $string2)
 * @method static find($delivery_boy_id)
 * @method static doesnthave(string $string)
 * @method static has(string $string)
 */
class DeliveryBoy extends Authenticatable
{
    use Notifiable,HasApiTokens, HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_boys';

    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'motorcycle_license_plate', 'latitude', 'longitude', 'is_free', 'is_offline', 'avatar_url', 'mobile',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'fcm_token', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new DeliveryBoyResetPasswordNotification($token));
    }

    public function getLocale(): string
    {
        return $this->locale ?? 'id';
    }

}
