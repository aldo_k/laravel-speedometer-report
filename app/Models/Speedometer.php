<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speedometer extends Model
{
    use HasFactory;

    protected $fillable = [
        'delivery_boy_id', 'date', 'start', 'start_at', 'start_file', 'finish', 'finish_at', 'finish_file', 'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime',
        'start_at' => 'datetime',
        'finish_at' => 'datetime',
    ];

    public function deliveryBoy()
    {
        return $this->hasOne('App\Models\DeliveryBoy', 'id')->select('name', 'motorcycle_license_plate');
    }

}
