<?php
namespace App\Helpers;


class TextUtil {


    public static $PLACEHOLDER_AVATAR_URL = "/placeholder_images/no_product.png";
    public static $PLACEHOLDER_PRODUCT_IMAGE_URL = "/placeholder_images/no_product.png";
    public static $PLACEHOLDER_SHOP_IMAGE_URL = "/placeholder_images/no_product.png";

    public static function getImageUrl($url,$placeholder=""): string
    {
        if($url)
            return asset('storage'.$url);
        return asset('storage'.$placeholder);
    }


    public static function getPhoneUrl($number): string
    {
        return "tel:".$number;
    }

    public static function getGoogleMapLocationUrl($latitude, $longitude): string
    {
        return "http://maps.google.com/maps?q=$latitude+$longitude";
    }

}

