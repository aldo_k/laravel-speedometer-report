<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1/user'], function () {

    //---------------- Auth --------------------//
    Route::post('/register', 'Api\v1\User\AuthController@register');
    Route::post('/login', 'Api\v1\User\AuthController@login');

    //Password  Reset
    Route::post('/password/email','Api\v1\User\ForgotPasswordController@sendResetLinkEmail');



    //Print Receipt
    Route::get('/orders/{id}/receipt','User\OrderReceiptController@show');



    Route::get('/app_data', 'Api\v1\User\UserController@getAppData');



    Route::group(['middleware' => ['auth:user-api']], function () {

        //---------------------------- App Data -------------------------//
        //App User Data
        Route::get('/app_data/user', 'Api\v1\User\UserController@getAppDataWithUser');



        //---------------------- Setting ----------------------------//
        Route::post('/update_profile', 'Api\v1\User\AuthController@updateProfile');

    });

    Route::get('/maintenance', function () {
        return response(['message' => ['Apps is now Online']], 200);
    });


});

Route::group(['prefix' => '/v1/delivery-boy'], function () {


    //App Data
    Route::get('/app_data', 'Api\v1\DeliveryBoy\DeliveryBoyController@getAppData');

      //---------------- Auth --------------------//
    Route::post('/register', 'Api\v1\DeliveryBoy\AuthController@register');
    Route::post('/login', 'Api\v1\DeliveryBoy\AuthController@login');

    //Password  Reset
    Route::post('/password/email','Api\v1\DeliveryBoy\ForgotPasswordController@sendResetLinkEmail');


    Route::group(['middleware' => ['auth:delivery-boy-api']], function () {

        //App User Data
        Route::get('/app_data/delivery_boy', 'Api\v1\DeliveryBoy\DeliveryBoyController@getAppDataWithDeliveryBoy');


        //---------------------- Setting ----------------------------//
        Route::post('/update_profile', 'Api\v1\DeliveryBoy\AuthController@updateProfile');


        //----------------------- Settings --------------------//
        Route::post('/change_status','Api\v1\DeliveryBoy\AuthController@changeStatus');

        //Reviews
        Route::get('/reviews', 'Api\v1\DeliveryBoy\ReviewController@index');

        //Speedometers
        Route::get('/speedometers', 'Api\v1\DeliveryBoy\SpeedometerController@index');
        Route::get('/speedometers/{id?}', 'Api\v1\DeliveryBoy\SpeedometerController@show');
        Route::post('/speedometers', 'Api\v1\DeliveryBoy\SpeedometerController@store');
        Route::put('/speedometers/{id?}', 'Api\v1\DeliveryBoy\SpeedometerController@update');

    });

    Route::get('/maintenance', function () {
        return response(['message' => [env('APP_NAME').' is now online']], 200);
    });

});
