<?php

use App\Helpers\TextUtil;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'locale'],function (){

    Route::get('/','User\Auth\LoginController@showLoginForm')->name('home');

    Route::prefix('user')->group(function (){

        Route::get('/login','User\Auth\LoginController@showLoginForm')->name('user.login');
        Route::post('/login','User\Auth\LoginController@login');
        // Route::post('/register','User\Auth\RegisterController@create')->name('user.register');


        //Password  Reset
        Route::post('/password/email','User\Auth\ForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
        Route::get('/password/reset','User\Auth\ForgotPasswordController@showLinkRequestForm')->name('user.password.request');
        Route::post('/password/reset','User\Auth\ResetPasswordController@reset');
        Route::get('/password/reset/{token}','User\Auth\ResetPasswordController@showResetForm')->name('user.password.reset');

    });

    Route::group(['middleware'=>'auth:user','prefix'=>'/user'],function (){

        //--------------------------- Auth -------------------------------------//
        Route::get('/logout','User\Auth\LoginController@logout')->name('user.logout');

        //-------------------------- User -----------------------------------//
        Route::get('/','User\UserController@index')->name('user.dashboard');
        Route::get('/setting','User\UserController@edit')->name('user.setting.edit');
        Route::patch('/setting','User\UserController@update')->name('user.setting.update');
        Route::patch('/setting/updateLocale/{langCode}','User\UserController@updateLocale')->name('user.setting.updateLocale');


        //------------------------------ Delivery Boy --------------------------//
        Route::get('/drivers','User\DeliveryBoyController@index')->name('user.drivers.index');
        Route::get('/drivers/add','User\DeliveryBoyController@create')->name('user.drivers.add');
        Route::post('/drivers/store','User\DeliveryBoyController@store')->name('user.drivers.store');
        Route::get('/drivers/{id?}/edit','User\DeliveryBoyController@edit')->name('user.drivers.edit');
        Route::post('/drivers/{id?}/edit','User\DeliveryBoyController@update')->name('user.drivers.update');
        Route::get('/drivers/{id?}/destroy','User\DeliveryBoyController@destroy')->name('user.drivers.destroy');

        //------------------------------ Speedometer --------------------------//
        Route::get('/speedometers','User\SpeedometerController@index')->name('user.speedometers.index');
        Route::get('/speedometers','User\SpeedometerController@index')->name('user.speedometers.index');
        Route::get('/speedometers/add','User\SpeedometerController@create')->name('user.speedometers.add');
        Route::post('/speedometers/store','User\SpeedometerController@store')->name('user.speedometers.store');
        Route::get('/speedometers/{id?}/edit','User\SpeedometerController@edit')->name('user.speedometers.edit');
        Route::post('/speedometers/{id?}/edit','User\SpeedometerController@update')->name('user.speedometers.update');
        Route::get('/speedometers/{id?}/destroy','User\SpeedometerController@destroy')->name('user.speedometers.destroy');
    });


    Route::prefix('user')->group(function (){
        //Password  Reset
        Route::post('/password/reset','User\Auth\ResetPasswordController@reset')->name('user.password.reset');
        Route::get('/password/reset/{token}','User\Auth\ResetPasswordController@showResetForm')->name('user.password.resetForm');
    });

    Route::prefix('delivery-boy')->group(function (){
        //Password  Reset
        Route::post('/password/reset','DeliveryBoy\Auth\ResetPasswordController@reset')->name('delivery-boy.password.reset');
        Route::get('/password/reset/{token}','DeliveryBoy\Auth\ResetPasswordController@showResetForm')->name('delivery-boy.password.resetForm');
    });

});